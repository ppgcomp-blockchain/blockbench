#!/bin/bash

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

# Setting up data dir
rm -rf $ETH_DATA  # Removing previous dir
mkdir -p $ETH_DATA  # Creating new dir

# Initializing database
geth init --datadir=$ETH_DATA $GENESIS_FILE

# Copying account private keys
cp -r keystore/* data/keystore/
