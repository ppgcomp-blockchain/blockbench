#!/bin/bash
cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo "++++ stop-all.sh"

i=0
while IFS="," read -r -u 3 addr user ssh_port ignored; do
  echo "** Client[$i] - $user@$addr:$ssh_port"
  echo "** Stopping driver"
  ssh -oStrictHostKeyChecking=no $user@$addr -p $ssh_port "killall -KILL driver" 
  echo "** done"
  let i=$i+1
done 3< <(tail -n +2 $CLIENTS)

i=0
while IFS="," read -r -u 3 addr user ssh_port ignored; do
  echo "** Host[$i] - $user@$addr:$ssh_port"
  echo "** Stopping geth"
  ssh -oStrictHostKeyChecking=no $user@$addr -p $ssh_port $ETH_HOME/stop.sh
  echo "** done"
  let i=$i+1
done 3< <(tail -n +2 $HOSTS)

echo "---- stop-all.sh"
