#!/bin/bash

NUM_HOSTS=$1

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo "++++ init-all.sh"

i=0
while IFS="," read -r -u 3 ADDR USER SSH_PORT _; do
  echo "** Host[$i] - $USER@$ADDR:$SSH_PORT"
  if [[ $i -lt $NUM_HOSTS ]]; then
    echo "** Sending genesis file"
    scp -P $SSH_PORT $GENESIS_FILE $USER@$ADDR:$GENESIS_FILE
    echo "** Initializing geth"
    ssh -oStrictHostKeyChecking=no $USER@$ADDR -p $SSH_PORT $ETH_HOME/init.sh
    echo "** done"
  fi
  let i=$i+1
done 3< <(tail -n +2 $HOSTS)

echo "---- init-all.sh"
