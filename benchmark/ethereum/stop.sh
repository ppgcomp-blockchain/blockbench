#!/bin/bash
cd `dirname ${BASH_SOURCE-$0}`
. env.sh

killall -KILL geth
killall -KILL realtime_comp_data.sh
rm -rf $ETH_DATA
rm -rf ~/.eth*
