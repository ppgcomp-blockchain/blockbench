#!/bin/bash

NUM_HOSTS=$1

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo '++++ deploy_contracts.sh'

# Longer wait time for contract mine
LONGER_WT=0
# Host addresses array
HOST_ADDRS=()
# Transaction receipts array
TRANSACTION_RECEIPTS=()
# Contract addresses array
CONTRACT_ADDRESSES=()

let i=0
while IFS="," read -r -u 3 CONTRACT_ID HOST_INDEX ACCOUNT_INDEX WT; do
  let AWK_HOST_INDEX=$HOST_INDEX+2

  echo "** Contract[$i] - $CONTRACT_ID -> Host[$HOST_INDEX]"
  
  if [[ $HOST_INDEX -lt $NUM_HOSTS ]]; then
    while IFS="," read -r -u3 HOST_ADDR HOST_SSH_USER HOST_SSH_PORT HOST_RPC_PORT _; do
      echo "** Deploying contract to $HOST_ADDR:$HOST_RPC_PORT"
      HOST_ADDRS+=( $HOST_ADDR:$HOST_RPC_PORT )
      TRANSACTION_RECEIPT=$(./deploy_contract_to_host.sh $CONTRACT_ID $HOST_ADDR $HOST_SSH_USER $HOST_SSH_PORT $HOST_RPC_PORT $ACCOUNT_INDEX)
      TRANSACTION_RECEIPTS+=( $TRANSACTION_RECEIPT )
      echo "** done $TRANSACTION_RECEIPT"
    done 3< <(awk "NR==$AWK_HOST_INDEX" $HOSTS)

    if [[ $LONGER_WT -lt $WT ]]; then
        LONGER_WT=$WT
    fi
  fi
  let i=$i+1
done 3< <(tail -n +2 $CONTRACTS)

echo "++ Waiting time for mining transactions ($LONGER_WT seconds)"

sleep $LONGER_WT

LENGTH=${#TRANSACTION_RECEIPTS[@]}

for (( i=0; i < ${LENGTH}; i++ )); do
  CONTRACT_ADDRESS=$(./get_contract_address.sh ${TRANSACTION_RECEIPTS[$i]} ${HOST_ADDRS[$i]})
  CONTRACT_ADDRESSES+=( $CONTRACT_ADDRESS )
done

# Creating contracts_data.csv file
echo "host_addr,deploy_transaction_receipt,contract_addr" > $CONTRACTS_DATA
for (( i=0; i < ${LENGTH}; i++ )); do
  echo "${HOST_ADDRS[$i]},${TRANSACTION_RECEIPTS[$i]},${CONTRACT_ADDRESSES[$i]}" >> $CONTRACTS_DATA
done

echo "++ Contracts data file generated"

echo '---- deploy_contracts.sh'
