#!/bin/bash
cd `dirname ${BASH_SOURCE-$0}`
. env.sh

HOST_INDEX=$1
CONSENSUS=$2
NODE_TYPE=$3
ACCOUNT_ADDR=$4

if [[ $CONSENSUS == "clique" ]]; then
  echo "Consensus: clique"
  
  if [[ $NODE_TYPE == "l" ]]; then # light (for clique its just a node that is not a miner)
    echo 'Starting light node'
    GETH_ARGS=''
  else # full
    echo 'Starting full node'
    GETH_ARGS="--miner.etherbase=$ACCOUNT_ADDR --mine --unlock $ACCOUNT_ADDR"  # The flag --password has to be on the main command below
  fi
else # ethash
  echo "Consensus: ethash"

  if [[ $NODE_TYPE == "l" ]]; then # light
    echo 'Starting light node'
    GETH_ARGS='--syncmode light'
  else # full
    echo 'Starting full node'
    GETH_ARGS="--mine --miner.threads 8 --miner.etherbase=$ACCOUNT_ADDR --light.serve 300 --light.nosyncserve --txlookuplimit 0 --unlock $ACCOUNT_ADDR"
  fi
fi

nohup geth --datadir=$ETH_DATA --nodiscover --http --http.addr 0.0.0.0 --http.port "8000" --http.corsdomain "*" -http.vhosts "*" --http.api eth,net,web3,personal,admin,txpool,debug --maxpeers 32 --networkid 9119 --allow-insecure-unlock --rpc.allow-unprotected-txs --verbosity 5 $GETH_ARGS --password <(echo -n "")  > $LOG_DIR/host_$HOST_INDEX 2>&1 &

sleep 1

for com in `cat $ETH_HOME/addPeer.txt`; do
  geth --exec $com attach http://localhost:8000
done

nohup ./realtime_comp_data.sh geth 1 > $LOG_DIR/host_$HOST_INDEX"_comp.csv" 2>&1 &
