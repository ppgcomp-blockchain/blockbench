#!/bin/bash

NUM_HOSTS=$1
NUM_CLIENTS=$2

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo "++++ start-multi-clients.sh $NUM_HOSTS $NUM_CLIENTS"

let i=0
while IFS="," read -r -u 3 CLIENT_INDEX HOST_INDEX ACCOUNT_INDEX THREADS TX_RATE CONTRACT_INDEX; do
  let AWK_CLIENT_INDEX=$CLIENT_INDEX+2
  let AWK_HOST_INDEX=$HOST_INDEX+2
  let AWK_CONTRACT_INDEX=$CONTRACT_INDEX+2

  echo "** Driver[$i] - Client[$CLIENT_INDEX] -> Host[$HOST_INDEX]/$CONTRACT_INDEX Threads: $THREADS Tx: $TX_RATE"
  
  if [[ $CLIENT_INDEX -lt $NUM_CLIENTS ]]; then
    if [[ $HOST_INDEX -lt $NUM_HOSTS ]]; then
      while IFS="," read -r -u3 CLIENT_ADDR CLIENT_USER CLIENT_SSH_PORT; do
        while IFS="," read -r -u3 HOST_ADDR HOST_USER HOST_SSH_PORT HOST_RPC_PORT _; do
          while IFS="," read -r -u3 _ _ CONTRACT_ADDR; do
            echo "** Getting account address"
            ACCOUNT_ADDR=$(./get_account_address.sh $ACCOUNT_INDEX)
            
            echo "** Unlocking account $ACCOUNT_ADDR"
            HOST_URL=$HOST_ADDR:$HOST_RPC_PORT
            ./unlock_account.sh $HOST_USER $HOST_ADDR $HOST_SSH_PORT $ACCOUNT_ADDR $HOST_URL
            
            echo "** Starting driver on $CLIENT_USER@$CLIENT_ADDR:$CLIENT_SSH_PORT"
            ssh -oStrictHostKeyChecking=no $CLIENT_USER@$CLIENT_ADDR -p $CLIENT_SSH_PORT $ETH_HOME/start-clients.sh $i $CLIENT_INDEX $HOST_INDEX $HOST_URL $THREADS $TX_RATE $CONTRACT_ADDR $ACCOUNT_ADDR
            
            echo "** done"
          done 3< <(awk "NR==$AWK_CONTRACT_INDEX" $CONTRACTS_DATA)
        done 3< <(awk "NR==$AWK_HOST_INDEX" $HOSTS)
      done 3< <(awk "NR==$AWK_CLIENT_INDEX" $CLIENTS)
    fi
  fi
  let i=$i+1
done 3< <(tail -n +2 $DRIVERS)

echo '---- start-multi-clients.sh'
