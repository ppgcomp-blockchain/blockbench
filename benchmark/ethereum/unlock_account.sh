#!/bin/bash

HOST_SSH_USER=$1
HOST_ADDR=$2
HOST_SSH_PORT=$3
ACCOUNT_ADDR=$4
HOST_URL=$5

ssh -oStrictHostKeyChecking=no $HOST_SSH_USER@$HOST_ADDR -p $HOST_SSH_PORT "geth --exec 'personal.unlockAccount(\"$ACCOUNT_ADDR\", \"\")' attach http://$HOST_URL"
