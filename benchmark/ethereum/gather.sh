#!/bin/bash

NUM_HOSTS=$1

cd `dirname ${BASH_SOURCE-$0}`
. env.sh 

echo '+++ gather.sh'

i=0
while IFS="," read -r -u 3 ADDR USER SSH_PORT _ ENODE_PORT _; do
  echo "** Host[$i] - $USER@$ADDR:$SSH_PORT"
  if [[ $i -lt $NUM_HOSTS ]]; then
    echo "** Getting node addr of $ADDR"
    echo "admin.addPeer("`ssh $USER@$ADDR -p $SSH_PORT $ETH_HOME/enode.sh $ADDR $ENODE_PORT 2>/dev/null | grep enode`")" >> addPeer.txt
    echo "** done"
  fi
  let i=$i+1
done 3< <(tail -n +2 $HOSTS)

echo "** Peers:"
cat addPeer.txt

echo '--- gather.sh'
