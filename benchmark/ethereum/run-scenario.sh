#!/bin/bash

SCENARIO_PATH=$1
NUM_HOSTS=$2
NUM_CLIENTS=$3
NUM_RUNS=$4

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

# Getting the name of the parameter files
HOSTS_FILENAME=$(basename $HOSTS)
CLIENTS_FILENAME=$(basename $CLIENTS)
DRIVERS_FILENAME=$(basename $DRIVERS)
CONTRACTS_FILENAME=$(basename $CONTRACTS)
GENESIS_FILE_FILENAME=$(basename $GENESIS_FILE)

# Calculating the path of the parameter files inside scenario folders 
SCENARIO_HOSTS="$SCENARIO_PATH/$HOSTS_FILENAME"
SCENARIO_CLIENTS="$SCENARIO_PATH/$CLIENTS_FILENAME"
SCENARIO_DRIVERS="$SCENARIO_PATH/$DRIVERS_FILENAME"
SCENARIO_CONTRACTS="$SCENARIO_PATH/$CONTRACTS_FILENAME"
SCENARIO_GENESIS_FILE="$SCENARIO_PATH/$GENESIS_FILE_FILENAME"

# Copying the parameter files of the scenario to the benchmark folder
cp $SCENARIO_HOSTS $HOSTS
cp $SCENARIO_CLIENTS $CLIENTS
cp $SCENARIO_DRIVERS $DRIVERS
cp $SCENARIO_CONTRACTS $CONTRACTS
cp $SCENARIO_GENESIS_FILE $GENESIS_FILE

# Getting the name of the scenario
SCENARIO_NAME=$(basename $SCENARIO_PATH)

# Running benchmark/scenario
./run-bench.sh $NUM_HOSTS $NUM_CLIENTS $NUM_RUNS $SCENARIO_NAME > bench

# Calculating the path of the logs folder for the scenario
LOGS_FOLDER="$LOG_DIR/$SCENARIO_NAME"

# Moving the logs of the benchmark and the genesis file to the logs folder
mv bench $LOGS_FOLDER
cp $GENESIS_FILE $LOGS_FOLDER
