#!/bin/bash

NUM_HOSTS=$1
NUM_CLIENTS=$2
NUM_RUNS=$3
FOLDER_NAME=$4

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

if [[ "$FOLDER_NAME" == "" ]]; then
    DATE=$(date +"%Y-%m-%d_%H-%M-%S")
    FOLDER_NAME=$BENCHMARK"_"$DATE
fi

for RUN in $(seq 1 $NUM_RUNS); do
    ./stop-all.sh 
    ./prepare_all_logs_folder.sh $NUM_HOSTS $NUM_CLIENTS
    ./init-all.sh $NUM_HOSTS
    ./start-all.sh $NUM_HOSTS

    let M=240+40*$NUM_HOSTS

    echo "++ Waiting time for host sync ($M seconds)"
    sleep $M

    ./deploy_contracts.sh $NUM_HOSTS

    ./start-multi-clients.sh $NUM_HOSTS $NUM_CLIENTS

    let M=$NUM_HOSTS*10+240
    echo "++ Running benchmark ($M seconds)"
    sleep $M

    ./log_final_comp_data-all.sh $NUM_HOSTS

    ./stop-all.sh

    ./get_all_logs.sh $NUM_HOSTS $NUM_CLIENTS $FOLDER_NAME $RUN

    sleep 5
done
