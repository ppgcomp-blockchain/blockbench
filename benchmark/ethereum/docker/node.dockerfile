FROM blockbench/ethereum/base:v0.1


# Instalando dependências
RUN apt -y update
RUN apt -y upgrade
RUN apt -y install openssh-server libtool build-essential automake
RUN apt -y clean


# Instalando dependências
RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install psmisc python3
RUN apt-get -y clean


# Configurando ssh
RUN mkdir /var/run/sshd
RUN echo 'root:mypassword' | chpasswd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -i 's/#PermitUserEnvironment no/PermitUserEnvironment yes/' /etc/ssh/sshd_config
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
RUN mkdir /root/.ssh
RUN echo "PATH=${PATH}" >> /root/.ssh/environment
EXPOSE 22


CMD ["/usr/sbin/sshd", "-D"]
