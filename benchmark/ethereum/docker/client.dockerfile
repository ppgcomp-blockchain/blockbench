FROM blockbench/ethereum/node:v0.1


# Instalando dependências
RUN apt -y update
RUN apt -y upgrade
RUN apt -y install libcurl4-openssl-dev
RUN apt -y clean


# Instalando o restclient-cpp com o patch do blockbench aplicado
WORKDIR /usr/local/src
RUN git clone https://github.com/mrtazz/restclient-cpp.git
WORKDIR /usr/local/src/restclient-cpp
RUN patch -p4 < $BLOCK_BENCH_HOME/benchmark/parity/patch_restclient
RUN ./autogen.sh
RUN ./configure
RUN make install
RUN ldconfig


# Instalando nvm
ENV NVM_DIR=/usr/local/nvm
ENV NODE_VERSION=16.14.0
RUN mkdir $NVM_DIR
RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default
ENV NODE_PATH=$NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH=$NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH


# Instalando dependências do node do blockbench
WORKDIR /workspaces/blockbench/src/micro/
RUN npm install -g lerna
RUN npm install


# Compilando drivers
WORKDIR /workspaces/blockbench/src/macro/kvstore/
RUN make
WORKDIR /workspaces/blockbench/src/macro/smallbank/
RUN make


# Atualizando PATH do ambiente do ssh
RUN echo "PATH=${PATH}" >> /root/.ssh/environment
