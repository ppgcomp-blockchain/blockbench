#!/bin/bash

TAG_VERSION=$1

if [[ "$TAG_VERSION" == "" ]]; then
    TAG_VERSION="v0.1"
fi

docker build -f benchmark/ethereum/docker/base.dockerfile -t blockbench/ethereum/base:$TAG_VERSION .
docker build -f benchmark/ethereum/docker/node.dockerfile -t blockbench/ethereum/node:$TAG_VERSION .
docker build -f benchmark/ethereum/docker/coordinator.dockerfile -t blockbench/ethereum/coordinator:$TAG_VERSION .
docker build -f benchmark/ethereum/docker/host.dockerfile -t blockbench/ethereum/host:$TAG_VERSION .
docker build -f benchmark/ethereum/docker/client.dockerfile -t blockbench/ethereum/client:$TAG_VERSION .
