#!/bin/bash

ADDR=$1

echo "addr,user,ssh_port,rpc_port,enode_port,node_type"

USER="root"
NODE_TYPE="f"

KEEP_RUNNING=1
let i=1
while [ $KEEP_RUNNING -gt 0 ]; do
    while read -r DOCKER_RESP; do
        HOST_PORT=$(echo "$DOCKER_RESP" | grep -E -o "^[0-9]+")
        CONTAINER_PORT=$(echo "$DOCKER_RESP" | grep -E -o "[0-9]+$")
        
        if [ $HOST_PORT -eq 22 ]; then
            SSH_PORT=$CONTAINER_PORT
        elif [ $HOST_PORT -eq 30303 ]; then
            ENODE_PORT=$CONTAINER_PORT
        elif [ $HOST_PORT -eq 8000 ]; then
            RPC_PORT=$CONTAINER_PORT
        fi
    done < <(docker port bb_host_$i 2> /dev/null)

    if [ "$SSH_PORT" != "" ] && [ "$ENODE_PORT" != "" ] && [ "$RPC_PORT" != "" ]; then
        echo "$ADDR,$USER,$SSH_PORT,$RPC_PORT,$ENODE_PORT,$NODE_TYPE"

        SSH_PORT=""
        ENODE_PORT=""
        ENODE_PORT=""
        let i=$i+1
    else
        KEEP_RUNNING=0
    fi
done
