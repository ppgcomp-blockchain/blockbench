#!/bin/bash

NUM_HOSTS=$1
NUM_CLIENTS=$2
ADDR=$3
TAG_VERSION=$4

if [[ "$TAG_VERSION" == "" ]]; then
    TAG_VERSION="v0.1"
fi

if [[ "$ADDR" == "" ]]; then
    # Default ip of the host on the container
    ADDR="172.17.0.1"
fi

cd `dirname ${BASH_SOURCE-$0}`
. ../env.sh

# Stopping and removing containers
docker stop bb_coord
docker rm bb_coord
docker ps --filter name=bb_host_* -aq | xargs docker stop | xargs docker rm
docker ps --filter name=bb_client_* -aq | xargs docker stop | xargs docker rm

# Running coordinator
docker run -dit --name bb_coord blockbench/ethereum/coordinator:$TAG_VERSION

# Running hosts
for i in `seq 1 $NUM_HOSTS`; do
    docker run -d -P --name bb_host_$i blockbench/ethereum/host:$TAG_VERSION
done

# Running clients
for i in `seq 1 $NUM_CLIENTS`; do
    docker run -d -P --name bb_client_$i blockbench/ethereum/client:$TAG_VERSION
done

# Updating project files
docker cp ../../.. bb_coord:/workspaces

for i in `seq 1 $NUM_HOSTS`; do
    docker cp ../../.. bb_host_$i:/workspaces
done

for i in `seq 1 $NUM_CLIENTS`; do
    docker cp ../../.. bb_client_$i:/workspaces
done

# Building hosts.csv file
./create_hosts_file.sh $ADDR > hosts.csv

# Sending hosts.csv file to coordinator
docker cp hosts.csv bb_coord:$HOSTS

# Building clients.csv file
./create_clients_file.sh $ADDR > clients.csv

# Sending clients.csv file to coordinator
docker cp clients.csv bb_coord:$CLIENTS

# Running ./setup-ssh-keys.sh /root/.ssh/id_rsa on coordinator
docker exec -it bb_coord $ETH_HOME/setup-ssh-keys.sh /root/.ssh/id_rsa
