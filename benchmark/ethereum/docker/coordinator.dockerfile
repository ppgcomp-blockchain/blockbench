FROM blockbench/ethereum/base:v0.1


# Installing dependencies
RUN apt -y update
RUN apt -y upgrade
RUN apt -y install curl
RUN apt -y clean

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get -y install jq
RUN apt-get -y clean


# Generating key pair
RUN ssh-keygen -f /root/.ssh/id_rsa -t rsa -b 2048 -N ""
