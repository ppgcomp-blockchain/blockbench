#!/bin/bash

ADDR=$1

echo "addr,user,ssh_port"

USER="root"
NODE_TYPE="f"

KEEP_RUNNING=1
let i=1
while [ $KEEP_RUNNING -gt 0 ]; do
    while read -r DOCKER_RESP; do
        SSH_PORT=$(echo "$DOCKER_RESP" | grep -E -o "[0-9]+$")
    done < <(docker port bb_client_$i 2> /dev/null)

    if [ "$SSH_PORT" != "" ]; then
        echo "$ADDR,$USER,$SSH_PORT"

        SSH_PORT=""
        let i=$i+1
    else
        KEEP_RUNNING=0
    fi
done
