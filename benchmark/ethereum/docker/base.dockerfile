FROM ubuntu:18.04

# Instalando dependências
RUN apt -y update
RUN apt -y upgrade
RUN apt -y install git
RUN apt -y clean

# Copiando código fonte do projeto
WORKDIR /workspaces/blockbench
COPY . .

ENV BLOCK_BENCH_HOME=/workspaces/blockbench
