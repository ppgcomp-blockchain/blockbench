FROM blockbench/ethereum/node:v0.1


# Instalando dependências
RUN apt -y update
RUN apt -y upgrade
RUN apt -y install wget
RUN apt -y clean


# Instalando golang
WORKDIR /workspaces/
RUN wget https://go.dev/dl/go1.19.3.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.19.3.linux-amd64.tar.gz
ENV PATH=/usr/local/go/bin:$PATH
RUN echo "export PATH=/usr/local/go/bin:${PATH}" >> /root/.bashrc
RUN rm go1.19.3.linux-amd64.tar.gz


# Instalando geth
RUN go install github.com/ethereum/go-ethereum/cmd/geth@v1.10.25
ENV PATH=/root/go/bin:$PATH
RUN echo "export PATH=/root/go/bin:${PATH}" >> /root/.bashrc
EXPOSE 30303
EXPOSE 8000


# Atualizando PATH do ambiente do ssh
RUN echo "PATH=${PATH}" >> /root/.ssh/environment
