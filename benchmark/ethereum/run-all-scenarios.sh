#!/bin/bash

NUM_HOSTS=$1
NUM_CLIENTS=$2
NUM_RUNS=$3

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

for SCENARIO in $SCENARIOS_DIR/*; do
    ./run-scenario.sh $SCENARIO $NUM_HOSTS $NUM_CLIENTS $NUM_RUNS
done
