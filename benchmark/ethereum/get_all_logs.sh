#!/bin/bash

NUM_HOSTS=$1
NUM_CLIENTS=$2
FOLDER_NAME=$3
RUN=$4

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo "++++ get_all_logs.sh"

mkdir -p $LOG_DIR/$FOLDER_NAME/$RUN

i=0
while IFS="," read -r -u 3 ADDR USER SSH_PORT _; do
  echo "** Client[$i] - $USER@$ADDR:$SSH_PORT"
  if [[ $i -lt $NUM_CLIENTS ]]; then
    echo "** Getting logs"
    scp -P $SSH_PORT $USER@$ADDR:$LOG_DIR/* $LOG_DIR/$FOLDER_NAME/$RUN
    echo "** done"
  fi
  let i=$i+1
done 3< <(tail -n +2 $CLIENTS)

i=0
while IFS="," read -r -u 3 ADDR USER SSH_PORT _; do
  echo "** Host[$i] - $USER@$ADDR:$SSH_PORT"
  if [[ $i -lt $NUM_HOSTS ]]; then
    echo "** Getting logs"
    scp -P $SSH_PORT $USER@$ADDR:$LOG_DIR/* $LOG_DIR/$FOLDER_NAME/$RUN
    echo "** done"
  fi
  let i=$i+1
done 3< <(tail -n +2 $HOSTS)

echo "---- get_all_logs.sh"
