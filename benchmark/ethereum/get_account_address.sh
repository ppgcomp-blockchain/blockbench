#!/bin/bash

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

ACCOUNT_INDEX=$1

ACCOUNT_ADDR=$(cat $GENESIS_FILE | jq -r ".alloc | keys_unsorted | .[$ACCOUNT_INDEX]")

echo 0x$ACCOUNT_ADDR
