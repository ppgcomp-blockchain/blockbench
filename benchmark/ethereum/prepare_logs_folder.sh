#!/bin/bash

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

mkdir -p $LOG_DIR

cd $LOG_DIR

FILE_COUNT=$(find . -maxdepth 1 -type f | wc -l)

# Testing if there are files to move to a folder
if [[ "$FILE_COUNT" -gt "0" ]]; then
    # Counting number of "old" folders
    FOLDERS_COUNT=$(find . -maxdepth 1 -type d -name "old\-*" | wc -l)
    let FOLDERS_COUNT+=1
    FOLDER_NAME="old-$FOLDERS_COUNT"

    # Creating folder
    mkdir $FOLDER_NAME

    # Moving files to the folder
    find . -maxdepth 1 -type f -exec mv -t $FOLDER_NAME {} +
fi
