#!/bin/bash
cd `dirname ${BASH_SOURCE-$0}`
. env.sh

i=0
while IFS="," read -r addr user ssh_port ignored; do
  echo "** Host[$i]: $host"
  ssh-copy-id -i $1 $user@$addr -p $ssh_port
  echo "** done"
  let i=$i+1
done < <(tail -n +2 $HOSTS)

i=0
while IFS="," read -r addr user ssh_port ignored; do
  echo "** Client[$i]: $client"
  ssh-copy-id -i $1 $user@$addr -p $ssh_port
  echo "** done"
  let i=$i+1
done < <(tail -n +2 $CLIENTS)
