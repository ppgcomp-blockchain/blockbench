#!/bin/bash

NUM_HOSTS=$1
NUM_CLIENTS=$2

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo "++++ prepare_all_logs_folder.sh"

i=0
while IFS="," read -r -u 3 addr user ssh_port ignored; do
  echo "** Host[$i] - $user@$addr:$ssh_port"
  if [[ $i -lt $NUM_HOSTS ]]; then
    echo "** Preparing logs folder"
    ssh -oStrictHostKeyChecking=no $user@$addr -p $ssh_port $ETH_HOME/prepare_logs_folder.sh
    echo "** done"
  fi
  let i=$i+1
done 3< <(tail -n +2 $HOSTS)

i=0
while IFS="," read -r -u 3 addr user ssh_port ignored; do
  echo "** Client[$i] - $user@$addr:$ssh_port"
  if [[ $i -lt $NUM_CLIENTS ]]; then
    echo "** Preparing logs folder"
    ssh -oStrictHostKeyChecking=no $user@$addr -p $ssh_port $ETH_HOME/prepare_logs_folder.sh
    echo "** done"
  fi
  let i=$i+1
done 3< <(tail -n +2 $CLIENTS)

echo "---- prepare_all_logs_folder.sh"
