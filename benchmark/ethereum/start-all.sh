#!/bin/bash

NUM_HOSTS=$1

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo '++++ start-all.sh'

rm -rf addPeer.txt
./gather.sh $NUM_HOSTS

sleep 3

i=0
while IFS="," read -r ADDR USER SSH_PORT _; do
  echo "** Host[$i] - $USER@$ADDR:$SSH_PORT"
  if [[ $i -lt $NUM_HOSTS ]]; then
    echo "** Sending addPeer.txt"
    scp -P $SSH_PORT addPeer.txt $USER@$ADDR:$ETH_HOME
    echo "** done"
  fi
  let i=$i+1
done < <(tail -n +2 $HOSTS)

sleep 3

CONSENSUS_DATA=$(cat $GENESIS_FILE | jq -r '.config.clique')

if [[ $CONSENSUS_DATA == "null" ]]; then
  CONSENSUS=ethash
else
  CONSENSUS=clique
fi

i=0
while IFS="," read -r -u 3 ADDR USER SSH_PORT _ _ NODE_TYPE ACCOUNT_INDEX; do
  echo "** Host[$i] - $USER@$ADDR:$SSH_PORT"
  if [[ $i -lt $NUM_HOSTS ]]; then
    echo "** Getting host main account address"
    ACCOUNT_ADDR=$(./get_account_address.sh $ACCOUNT_INDEX)
    echo "** Start mining"
    ssh -oStrictHostKeyChecking=no $USER@$ADDR -p $SSH_PORT $ETH_HOME/start-mining.sh $i $CONSENSUS $NODE_TYPE $ACCOUNT_ADDR
    echo "** done"
  fi
  let i=$i+1
done 3< <(tail -n +2 $HOSTS)

echo '---- start-all.sh'
