#!/bin/bash

DRIVER_INDEX=$1
CLIENT_INDEX=$2
HOST_INDEX=$3
HOST_ENDPOINT=$4
NUM_THREADS=$5
TX_RATE=$6
CONTRACT_ADDR=$7
ACCOUNT_ADDR=$8

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo "+++ start-clients.sh $DRIVER_INDEX $CLIENT_INDEX $HOST_INDEX $HOST_ENDPOINT $NUM_THREADS $TX_RATE $CONTRACT_ADDR $ACCOUNT_ADDR"

echo "** Starting process to connect to Host[$HOST_INDEX] $HOST_ENDPOINT"
nohup $EXE_HOME/driver -db ethereum -threads $NUM_THREADS -P $EXE_HOME/../kvstore/workloads/workloada.spec -endpoint $HOST_ENDPOINT -txrate $TX_RATE -contract_addr $CONTRACT_ADDR -account_addr $ACCOUNT_ADDR -client_log_file $LOG_DIR/driver_$DRIVER_INDEX"_"client_$CLIENT_INDEX"_"host_$HOST_INDEX"_"threads_$NUM_THREADS"_"TXRATE_$TX_RATE"_client.csv" -network_log_file $LOG_DIR/driver_$DRIVER_INDEX"_"client_$CLIENT_INDEX"_"host_$HOST_INDEX"_"threads_$NUM_THREADS"_"TXRATE_$TX_RATE"_network.csv" -txs_log_file $LOG_DIR/driver_$DRIVER_INDEX"_"client_$CLIENT_INDEX"_"host_$HOST_INDEX"_"threads_$NUM_THREADS"_"TXRATE_$TX_RATE"_txs.csv" > $LOG_DIR/driver_$DRIVER_INDEX 2>&1 &
echo "** done"

echo "--- start-clients.sh"
