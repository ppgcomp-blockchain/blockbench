#!/bin/bash

TRANSACTION_RECEIPT=$1
HOST_ADDR=$2

POST_DATA='{'
POST_DATA+='"jsonrpc":"2.0",'
POST_DATA+='"method":"eth_getTransactionReceipt",'
POST_DATA+='"params":["'$TRANSACTION_RECEIPT'"],'
POST_DATA+='"id":1'
POST_DATA+='}'

RESULT=$(curl -H 'Content-Type: application/json' -X POST --data "$POST_DATA" $HOST_ADDR 2> /dev/null)

CONTRACT_ADDR=$(echo $RESULT | jq -r ".result.contractAddress")

echo $CONTRACT_ADDR
