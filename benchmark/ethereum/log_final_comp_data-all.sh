#!/bin/bash

NUM_HOSTS=$1

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

echo '++++ log_final_comp_data-all.sh'

i=0
while IFS="," read -r -u 3 ADDR USER SSH_PORT _ _ NODE_TYPE; do
  echo "** Host[$i] - $USER@$ADDR:$SSH_PORT"
  if [[ $i -lt $NUM_HOSTS ]]; then
    echo "** Logging final computational data"
    ssh -oStrictHostKeyChecking=no $USER@$ADDR -p $SSH_PORT $ETH_HOME/log_final_comp_data.sh $i geth
    echo "** done"
  fi
  let i=$i+1
done 3< <(tail -n +2 $HOSTS)

echo '---- log_final_comp_data-all.sh'
