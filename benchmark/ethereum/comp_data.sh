#!/bin/bash

# Baseado no script contido em https://www.baeldung.com/linux/total-process-cpu-usage
PID=$1


PROCESS_STAT=($(sed -E 's/\([^)]+\)/X/' "/proc/$PID/stat"))
PROCESS_UTIME=${PROCESS_STAT[13]}
PROCESS_STIME=${PROCESS_STAT[14]}
PROCESS_STARTTIME=${PROCESS_STAT[21]}
PROCESS_VSIZE=${PROCESS_STAT[22]}
PROCESS_RSS=${PROCESS_STAT[23]}
SYSTEM_UPTIME_SEC=$(tr . ' ' </proc/uptime | awk '{print $1}')

CLK_TCK=$(getconf CLK_TCK)

echo $SYSTEM_UPTIME_SEC,$CLK_TCK,$PROCESS_STARTTIME,$PROCESS_UTIME,$PROCESS_STIME,$PROCESS_VSIZE,$PROCESS_RSS
