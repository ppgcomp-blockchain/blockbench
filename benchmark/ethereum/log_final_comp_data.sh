#!/bin/bash

HOST_INDEX=$1
PROCESS_NAME=$2

cd `dirname ${BASH_SOURCE-$0}`
. env.sh

# PID do processo
PID=$(pidof $PROCESS_NAME)

cat /proc/$PID/status > $LOG_DIR/host_$HOST_INDEX"_final_comp"
