#!/bin/bash
cd `dirname ${BASH_SOURCE-$0}`
. env.sh 

addr=$1
port=$2

geth --datadir=$ETH_DATA --http --http.addr 0.0.0.0 --http.port "8000" --http.corsdomain "*" --miner.gasprice 0 --networkid 9119 --allow-insecure-unlock --unlock 0 --password <(echo -n "") --exec "console.log(admin.nodeInfo.enode);" console 2>/dev/null | grep enode | perl -pe "s/(?<=@)([a-zA-Z0-9.]+:[0-9]+)/$addr:$port/g" | perl -pe "s/^/\"/; s/\s*$/\"/;"
